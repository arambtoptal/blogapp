import {
  TOPIC_SELECTED,
  TOPICS_FETCH_INIT,
  TOPICS_FETCH_SUCCESS,
  TOPICS_FETCH_FAIL,
} from './types';
import _ from 'lodash';
import firebase from 'firebase';

export const topicsFetch = () => {
  return dispatch => {
    dispatch({ type: TOPICS_FETCH_INIT });
    firebase.database().ref('/topics').on('value', snapshot => {
      dispatch({ type: TOPICS_FETCH_SUCCESS, payload: snapshot.val() });
    }, error => {
      console.log('firebase error while fetching the topics', error);
      dispatch({ type: TOPICS_FETCH_FAIL, payload: error });
    });
  }
}

export const topicSelected = topicId => {
  return { type: TOPIC_SELECTED, payload: topicId };
}
