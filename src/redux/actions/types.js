export const LOGOUT_USER_SUCCESS = 'logout_user_success';

export const USER_IS_LOGGED_IN_CHECK = 'user_is_logged_in_check';
export const USER_IS_LOGGED_IN = 'user_is_logged_in';
export const LOGIN_VALUE_ENTER = 'login_value_enter';
export const LOGIN = 'login';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_FAIL = 'login_fail';

export const POST_VALUE_UPDATE = 'post_value_update';
export const POST_UPDATE = 'post_update';
export const POST_UPDATE_CANCEL = 'post_update_cancel';
export const POST_CREATE = 'post_create';
export const POST_SAVE_SUCCESS = 'post_save_success';
export const POST_DELETE_SUCCESS = 'post_delete_success';
export const POST_DELETE_FAIL = 'post_delete_fail';

export const POSTS_FETCH_INIT = 'posts_fetch_init';
export const POSTS_FETCH_SUCCESS = 'posts_fetch_success';
export const POSTS_FETCH_FAIL = 'posts_fetch_fail';

export const PROFILE_UPDATE = 'profile_update';
export const PROFILE_SAVE_SUCCESS = 'profile_save_success';

export const REGISTER_VALUE_ENTER = 'register_value_enter';
export const REGISTER = 'register';
export const REGISTER_SUCCESS = 'register_success';
export const REGISTER_FAIL= 'register_fail';

export const TOPIC_SELECTED = 'topic_selected';
export const TOPICS_FETCH_INIT = 'topics_fetch_init';
export const TOPICS_FETCH_SUCCESS = 'topics_fetch_success';
export const TOPICS_FETCH_FAIL = 'topics_fetch_fail';
