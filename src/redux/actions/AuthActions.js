import {
  LOGOUT_USER_SUCCESS,
  USER_IS_LOGGED_IN_CHECK,
  USER_IS_LOGGED_IN,
  LOGIN_VALUE_ENTER,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  REGISTER_VALUE_ENTER,
  REGISTER,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
} from './types';
import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

// Error message we will use in case there is no message supplied by firebase.
const GENERIC_ERROR_MESSAGE = 'An error occurred. Please check your input.';

/**
 * Login related actions.
 */

export const userIsLoggedIn = () => {
  return dispatch => {
    dispatch({ type: USER_IS_LOGGED_IN_CHECK });
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      .then(() => {
        firebase.auth().onAuthStateChanged(user => {
          dispatch({ type: USER_IS_LOGGED_IN, payload: Boolean(user) });
        });
      })
      .catch(error => {
        console.log('firebase error while checking if the user is logged in', error);
      });
  };
}

export const loginValueEnter = ({ prop, value }) => {
  return { type: LOGIN_VALUE_ENTER, payload: { prop, value } };
}

export const login = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: LOGIN });
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => {
        loginUserSuccess(dispatch, user)
      })
      .catch(error => {
        loginFail(dispatch, error);
      });
  };
}

export const loginFail = (dispatch, error) => {
  const message = (error.message !== undefined) ? error.message : GENERIC_ERROR_MESSAGE;

  dispatch({ type: LOGIN_FAIL, payload: message });
}

export const loginSuccess = (dispatch, user) => {
  dispatch({ type: LOGIN_SUCCESS, payload: user });
  Actions.pop('profile');
}

const loginUserSuccess = (dispatch, user) => {
  Actions.pop('main');
  dispatch({ type: LOGIN_SUCCESS, payload: user });
}

/**
 * Registration related actions.
 */

export const registerValueEnter = ({ prop, value }) => {
  return { type: REGISTER_VALUE_ENTER, payload: { prop, value } };
}

export const register = ({email, password}) => {
  return (dispatch) => {
    dispatch({ type: REGISTER });
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(user => {
        loginUserSuccess(dispatch, user);
      })
      .catch(error => {
        registerUserFail(dispatch, error);
      });
  }
}

export const registerSuccess = (dispatch, user) => {
  dispatch({ type: REGISTER_SUCCESS, payload: user });
  Actions.pop('profile');
}

const registerUserFail = (dispatch, error) => {
  const message = (error.message !== undefined) ? error.message : GENERIC_ERROR_MESSAGE;
  dispatch({ type: REGISTER_FAIL, payload: message });
}

/**
 * Logout.
 */

export const logoutUser = dispatch => {
  return (dispatch) => {
    firebase.auth().signOut()
      .then(() => {
        dispatch({ type: LOGOUT_USER_SUCCESS });
        Actions.pop('profile');
      })
      .catch(error => {
        console.log('firebase error while logging the user out', error);
      });
  }
}
