import firebase from 'firebase';
import { Platform } from 'react-native';
import {
  POST_VALUE_UPDATE,
  POST_UPDATE_CANCEL,
  POST_CREATE,
  POST_SAVE_SUCCESS,
  POSTS_FETCH_INIT,
  POSTS_FETCH_SUCCESS,
  POSTS_FETCH_FAIL,
  POST_DELETE_SUCCESS,
  POST_DELETE_FAIL,
} from './types';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

export const postValueUpdate = ({ prop, value }) => {
  return { type: POST_VALUE_UPDATE, payload: { prop, value }};
}

export const postUpdateCancelled = () => {
  return { type: POST_UPDATE_CANCEL };
}

export const postCreate = ({ title, body, topicId, image }) => {
  const { currentUser } = firebase.auth();
  const imagePath = image !== null ? image.metadata.fullPath : null;

  return dispatch => {
    firebase.database().ref('/posts')
      .push({ title, body, topicId, image: imagePath, uid: currentUser.uid, date: new Date })
      .then(() => {
        dispatch({ type: POST_CREATE });
        Actions.pop();
      })
      .catch((error) => console.log('postCreate firebase error:', error));
  }
}

export const postUpdate = ({ postId, title, body, topicId, image }) => {
  let imagePath = null;

  if (image !== null) {
    if (image.metadata !== undefined && image.metadata.fullPath !== undefined) {
      imagePath = image.metadata.fullPath;
    }
    else {
      imagePath = image;
    }
  }

  return dispatch => {
    firebase.database().ref(`/posts/${postId}`)
      .update({ title, body, topicId, image: imagePath })
      .then(() => {
        dispatch({ type: POST_SAVE_SUCCESS });
        Actions.pop();
      })
      .catch(error => console.log('postUpdate firebase error', error));
  }
}

export const postsFetch = (selectedTopic = null) => {
  return (dispatch) => {
    dispatch({ type: POSTS_FETCH_INIT });
    if (selectedTopic !== null) {
      firebase.database().ref('/posts').orderByChild('topicId').equalTo(selectedTopic).once('value').then(postSnap => {
        dispatch({ type: POSTS_FETCH_SUCCESS, payload: postSnap.val() });
      }, error => {
        console.log('firebase error while fetching the filtered posts:', error);
        dispatch({ type: POSTS_FETCH_FAIL, payload: error });
      });
    }
    else {
      firebase.database().ref('/posts').on('value', postSnap => {
        dispatch({ type: POSTS_FETCH_SUCCESS, payload: postSnap.val() });
      }, error => {
        console.log('firebase error while fetching the posts:', error);
        dispatch({ type: POSTS_FETCH_FAIL, payload: error });
      });
    }
  }
}

export const postDeleteData = ({ postId, image }) => {
  console.log('should delete post', postId);
  return (dispatch) => {
    if (image !== null && image !== undefined) {
      firebase.storage().ref(image).delete()
        .then(() => {
          postDelete({ postId });
        })
        .catch(error => {
          console.log('error deleting image', error);
        });
    }
    else {
      postDelete({ postId });
    }
  }
}

export const postDelete = ({ postId, image }) => {
  firebase.database().ref(`/posts/${postId}`)
    .remove()
    .then(() => {
      console.log('post', postId, 'deleted successfully');
      Actions.pop();
      dispatch({ type: POST_DELETE_SUCCESS });
    })
    .catch((error) => {
      dispatch({ TYPE: POST_DELETE_FAIL });
      console.log('postDelete firebase error:', error);
    });
}
