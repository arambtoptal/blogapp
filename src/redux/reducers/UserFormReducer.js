import {
  USER_UPDATE,
  USER_CREATE,
  USER_CREATE_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  email: '',
  password: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_UPDATE:
      return { ...state, [action.payload.props]: action.payload.value };

    case USER_CREATE:
    case USER_CREATE_SUCCESS:
      return INITIAL_STATE;

    default:
      return state;
  }
}
