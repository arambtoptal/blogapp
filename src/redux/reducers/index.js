import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import PostFormReducer from './PostFormReducer';
import PostReducer from './PostReducer';
import ProfileFormReducer from './ProfileFormReducer';
import UserFormReducer from './UserFormReducer';
import TopicReducer from './TopicReducer';

export default combineReducers({
  auth: AuthReducer,
  postForm: PostFormReducer,
  posts: PostReducer,
  topics: TopicReducer,
  profileForm: ProfileFormReducer,
  userForm: UserFormReducer,
});
