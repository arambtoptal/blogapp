import {
  PROFILE_UPDATE,
  PROFILE_SAVE_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  email: '',
  password: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PROFILE_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };

    default:
      return state;
  }
};
