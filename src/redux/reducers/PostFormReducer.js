import {
  POST_VALUE_UPDATE,
  POST_UPDATE,
  POST_UPDATE_CANCEL,
  POST_CREATE,
  POST_SAVE_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  title: '',
  body: '',
  image: null,
  topicId: '-LSiYf02DmyzG1PFsgnH',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case POST_VALUE_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };

    case POST_CREATE:
    case POST_UPDATE:
    case POST_SAVE_SUCCESS:
    case POST_UPDATE_CANCEL:
      return INITIAL_STATE;

    default:
      return state;
  }
}
