import {
  TOPICS_FETCH_INIT,
  TOPICS_FETCH_SUCCESS,
  TOPICS_FETCH_FAIL,
  TOPIC_SELECTED,
} from '../actions/types';

const INITIAL_STATE = {
  topics: [],
  loading: false,
  error: '',
  selectedTopic: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TOPIC_SELECTED:
      return { ...state, selectedTopic: action.payload };

    case TOPICS_FETCH_INIT:
      return { ...state, loading: true };

    case TOPICS_FETCH_SUCCESS:
      return { ...state, loading: false, topics: action.payload, error: '' };

    case TOPICS_FETCH_FAIL:
      return { ...state, loading: false, error: action.payload };

    default:
      return state;
  }
}
