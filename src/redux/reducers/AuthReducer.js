import {
  LOGOUT_USER_SUCCESS,
  USER_IS_LOGGED_IN_CHECK,
  USER_IS_LOGGED_IN,
  LOGIN_VALUE_ENTER,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  REGISTER_VALUE_ENTER,
  REGISTER,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  loggedIn: null,
  checkingIfLoggedIn: false,
  email: '',
  password: '',
  user: null,
  error: '',
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_IS_LOGGED_IN_CHECK:
      return { ...state, ...INITIAL_STATE, checkingIfLoggedIn: true };

    case USER_IS_LOGGED_IN:
      return { ...state, ...INITIAL_STATE, loggedIn: Boolean(action.payload), checkingIfLoggedIn: false };

    case LOGIN_VALUE_ENTER:
    case REGISTER_VALUE_ENTER:
      return { ...state, [action.payload.prop]: action.payload.value, error: '' };

    case LOGIN:
    case REGISTER:
      return { ...state, loading: true, error: '' };

    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
      return { ...state, ...INITIAL_STATE, user: action.payload, loggedIn: true };

    case LOGIN_FAIL:
      return { ...state, error: action.payload, password: '', loading: false };

    case REGISTER_FAIL:
      return { ...state, error: action.payload, password: '', loading: false };

    case LOGOUT_USER_SUCCESS:
      return INITIAL_STATE;

    default:
      return state;
  }
};
