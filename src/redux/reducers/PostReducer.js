import {
  POSTS_FETCH_INIT,
  POSTS_FETCH_SUCCESS,
  POSTS_FETCH_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  loading: false,
  posts: [],
  error: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case POSTS_FETCH_INIT:
      return { ...state, loading: true };

    case POSTS_FETCH_SUCCESS:
      return { ...state, loading: false, posts: action.payload, error: '' };

    case POSTS_FETCH_FAIL:
      return { ...state, loading: false, error: action.payload };

    default:
      return state;
  }
}
