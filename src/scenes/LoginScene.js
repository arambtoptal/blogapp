import React, { Component } from 'react';
import LoginPage from '../components/LoginPage';

class LoginScene extends Component {
  render() {
    return (
      <LoginPage />
    );
  }
};

export default LoginScene;
