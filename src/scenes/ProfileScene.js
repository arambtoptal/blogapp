import React, { Component } from 'react';
import AnonymousProfilePage from '../components/AnonymousProfilePage';
import LoggedProfilePage from '../components/LoggedProfilePage';
import { connect } from 'react-redux';
import { userIsLoggedIn } from '../redux/actions';
import { Spinner } from '../components/common';

class ProfileScene extends Component {
  render() {
    if (this.props.checkingIfLoggedIn) {
      return <Spinner />
    }

    if (this.props.loggedIn) {
      return (
        <LoggedProfilePage />
      );
    }

    return (
      <AnonymousProfilePage />
    );
  }
};

const mapStateToProps = state => {
  const { loggedIn, checkingIfLoggedIn } = state.auth;

  return { loggedIn, checkingIfLoggedIn };
}

export default connect(mapStateToProps, { userIsLoggedIn })(ProfileScene);