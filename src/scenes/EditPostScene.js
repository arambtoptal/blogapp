import React, { Component } from 'react';
import BlogPostUpdate from '../components/BlogPostUpdate';
import { connect } from 'react-redux';
import { postUpdate, postDeleteData } from '../redux/actions';

class EditPostScene extends Component {
  render() {
    return (
      <BlogPostUpdate { ...this.props } />
    );
  }
};

export default connect(null, { postUpdate, postDeleteData })(EditPostScene);