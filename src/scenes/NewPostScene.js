import React, { Component } from 'react';
import BlogPostCreate from '../components/BlogPostCreate';

class NewPostScene extends Component {
  render() {
    return (
      <BlogPostCreate />
    );
  }
};

export default NewPostScene;