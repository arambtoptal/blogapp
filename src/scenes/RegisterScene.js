import React, { Component } from 'react';
import RegisterPage from '../components/RegisterPage';

class RegisterScene extends Component {
  render() {
    return (
      <RegisterPage />
    );
  }
};

export default RegisterScene;
