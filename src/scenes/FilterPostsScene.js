import React, { Component } from 'react';
import { ScrollView, Text, TouchableOpacity, SafeAreaView, FlatList } from 'react-native';
import { Card, CardSection } from '../components/common';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { topicSelected } from '../redux/actions';
import _ from 'lodash';

class FilterPostsScene extends Component {
  onPressCancel() {
    Actions.pop();
  }

  onPressReset() {
    this.props.topicSelected(null);
    Actions.pop();
  }

  renderTopic(topic) {
    let topicStyle;
    if (this.props.selectedTopic === topic.key) {
      topicStyle = {fontSize: 16, fontWeight: '600', color: '#007aff'};
    }
    else {
      topicStyle = {fontSize: 16};
    }

    return (
      <TouchableOpacity onPress={ () => {
        this.props.topicSelected(topic.key);
        Actions.pop();
      }}>
        <CardSection>
          <Text style={ topicStyle }>{ topic.title }</Text>
        </CardSection>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView>
          <Card>
            <CardSection>
              <Text style={{ fontSize: 20 }}>Filter posts by topic</Text>
            </CardSection>
            <FlatList data={ this.props.topics } renderItem={ topic => this.renderTopic(topic.item) } />
          </Card>
          <Card>
            <CardSection>
              <TouchableOpacity onPress={ this.onPressReset.bind(this) } style={ styles.nonPrimaryButton }>
                <Text style={ styles.nonPrimaryButtonText }>Reset</Text>
              </TouchableOpacity>
            </CardSection>
            <CardSection>
              <TouchableOpacity onPress={ this.onPressCancel.bind(this) } style={ styles.nonPrimaryButton }>
                <Text style={ styles.nonPrimaryButtonText }>Cancel</Text>
              </TouchableOpacity>
            </CardSection>
          </Card>
        </ScrollView>
      </SafeAreaView>
    );
  }
};

const styles = {
  nonPrimaryButton: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#FFF',
  },
  nonPrimaryButtonText: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 16,
    fontWeight: '600',
  },
};

const mapStateToProps = state => {
  const topics = _.map(state.topics.topics, (val, topicId) => {
    return { ...val, key: topicId };
  });

  return { topics, selectedTopic: state.topics.selectedTopic };
}

export default connect(mapStateToProps, { topicSelected })(FilterPostsScene);