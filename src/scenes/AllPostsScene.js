import React, { Component } from 'react';
import AllPostsPage from '../components/AllPostsPage';

class AllPostsScene extends Component {
  render() {
    return (
      <AllPostsPage />
    );
  }
};

export default AllPostsScene;
