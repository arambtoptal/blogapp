import React, { Component } from 'react';
import {
  View,
  Text,
  Alert,
} from 'react-native';
import { Actions, Router, Scene } from 'react-native-router-flux';
import AllPostsScene from './scenes/AllPostsScene';
import NewPostScene from './scenes/NewPostScene';
import EditPostScene from './scenes/EditPostScene';
import ProfileScene from './scenes/ProfileScene';
import LoginScene from './scenes/LoginScene';
import RegisterScene from './scenes/RegisterScene';
import FilterPostsScene from './scenes/FilterPostsScene';
import { connect } from 'react-redux';
import { userIsLoggedIn, postUpdateCancelled } from './redux/actions';

const TabIcon = ({selected, title}) => {
  return <Text />;
}

import firebase from 'firebase';

const transitionConfig = () => ({
  screenInterpolator: StackViewStyleInterpolator.forFadeFromBottomAndroid,
});

class AppRouter extends Component {
  componentWillMount() {
    this.props.userIsLoggedIn();
  }

  onPressEditPostCancel() {
    this.props.postUpdateCancelled();
    Actions.pop();
  }

  onPressAddPost() {
    if (this.props.loggedIn) {
      Actions.newPost();
    }
    else {
      Alert.alert('Please login', 'You have to login or register in order to submit new blog posts.', [
        { text: 'OK', onPress: () => {}, style: 'cancel' },
      ]);
    }
  }

  render() {
    return (
      <Router>
        <Scene key="root" hideNavBar>
          <Scene key="tabbar" tabs>
            <Scene key="main" title="Posts" icon={TabIcon}>
              <Scene key="allPosts" component={AllPostsScene} title="Blog Posts" rightTitle="Add" onRight={this.onPressAddPost.bind(this)} leftTitle="Filter" onLeft={() => Actions.filterPosts()} />
              <Scene key="newPost" component={NewPostScene} title="New Post" leftTitle="Cancel" onLeft={() => this.onPressEditPostCancel()} />
              <Scene key="editPost" component={EditPostScene} title="Edit Post" leftTitle="Cancel" onLeft={() => this.onPressEditPostCancel()} />
            </Scene>
            <Scene key="user" title="Profile" icon={TabIcon}>
              <Scene key="profile" component={ProfileScene} title="Profile" />
              <Scene key="login" component={LoginScene} title="Login" />
              <Scene key="register" component={RegisterScene} title="Register" />
            </Scene>
          </Scene>
          <Scene key="filterPosts" component={FilterPostsScene} title="Filter Posts" />
        </Scene>
      </Router>
    );
  }
};

const mapStateToProps = state => {
  const { loggedIn } = state.auth;

  return {
    loggedIn,
    userIsLoggedIn,
    postUpdateCancelled,
  };
};

export default connect(mapStateToProps, { userIsLoggedIn, postUpdateCancelled })(AppRouter);
