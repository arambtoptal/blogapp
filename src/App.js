import React, { Component } from 'react';
import firebase from 'firebase';
import AppRouter from './AppRouter';
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './redux/reducers';
import ReduxThunk from 'redux-thunk';
import config from './config/config.json';

class App extends Component {
  componentWillMount() {
    firebase.initializeApp(config.firebase);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
        <AppRouter />
      </Provider>
    );
  }
};

export default App;
