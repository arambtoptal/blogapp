import React, { Component } from 'react';
import { Alert, ScrollView } from 'react-native';
import { Card, CardSection, Button } from '../common';
import BlogPostForm from '../BlogPostForm';
import { connect } from 'react-redux';
import { postCreate, postValueUpdate } from '../../redux/actions';

class BlogPostCreate extends Component {
  onPressCreate() {
    let { title, body, topicId, image } = this.props;
    // Make sure that the title and body are filled in.
    if (!title.length || !body.length) {
      Alert.alert(
        'Form error',
        'Post title and body fields are required.',
        [{ text: 'OK', style: 'cancel' }],
      );
    }
    else {
      this.props.postCreate({ title, body, topicId, image });
    }
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <Card>
          <BlogPostForm { ...this.props } />
          <CardSection>
            <Button onPress={ this.onPressCreate.bind(this) }>
              Create
            </Button>
          </CardSection>
        </Card>
      </ScrollView>
    );
  }
};

const mapStateToProps = state => {
  const { title, body, topicId, image } = state.postForm;

  return { title, body, topicId, image };
}

export default connect(mapStateToProps, { postCreate, postValueUpdate})(BlogPostCreate);
