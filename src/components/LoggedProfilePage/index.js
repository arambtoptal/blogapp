import React, { Component } from 'react';
import { Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button, Card, CardSection } from '../common';
import firebase from 'firebase';
import { connect } from 'react-redux';
import { logoutUser, userIsLoggedIn } from '../../redux/actions';

class LoggedProfilePage extends Component {
  onPressLogout() {
    this.props.logoutUser();
  }

  render() {
    const { currentUser } = firebase.auth();

    return (
      <Card>
        <CardSection>
          <Text style={{ fontSize: 18 }}>You are logged in</Text>
        </CardSection>
        <CardSection>
          <Text>Email address: { currentUser.email }</Text>
        </CardSection>
        <CardSection>
          <Text>You can now create new blog posts that will be visible for all users of the app.</Text>
        </CardSection>
        <CardSection>
          <Button onPress={this.onPressLogout.bind(this)}>
            Logout
          </Button>
        </CardSection>
      </Card>
    );
  }
};

export default connect(null, { logoutUser })(LoggedProfilePage);