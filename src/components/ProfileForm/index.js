import React, { Component } from 'react';
import { View } from 'react-native';
import { Input, CardSection } from '../common';
import { connect } from 'react-redux';
import { registerUser, registerValueEnter } from '../../redux/actions';

class ProfileForm extends Component {
  render() {
    return (
      <View>
        <CardSection>
          <Input
            label="Email"
            placeholder="email@address.com"
            onChangeText={text => this.props.registerValueEnter({ prop: 'email', value: text })}
            value={this.props.email}
            autoCapitalize='none'
          />
        </CardSection>
        <CardSection>
          <Input
            label="Password"
            placeholder="password"
            onChangeText={text => this.props.registerValueEnter({ prop: 'password', value: text })}
            value={this.props.password}
            autoCapitalize='none'
            secureTextEntry
          />
        </CardSection>
      </View>
    );
  }
};

const mapStateToProps = state => {
  const { email, password } = state.profileForm;

  return {
    email: state.auth.email,
    password: state.auth.password,
    error: state.auth.error,
    loading: state.auth.loading,
  };
}

export default connect(mapStateToProps, { registerUser, registerValueEnter })(ProfileForm);
