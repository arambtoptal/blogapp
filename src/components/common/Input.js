import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
} from 'react-native';

class Input extends Component {
  render() {
    const {
      label,
      customStyles,
    } = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.label}>{label}</Text>
        <TextInput style={[ styles.input, customStyles ]} { ...this.props } />
      </View>
    );
  }
};

const styles = {
  label: {
    fontSize: 18,
    paddingLeft: 5,
    flex: 1,
  },
  input: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18,
    lineHeight: 23,
    flex: 2,
  },
  container: {
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
};

export { Input };