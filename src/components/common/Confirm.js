import React, { Component } from 'react';
import {
  View,
  Text,
  Modal,
} from 'react-native';
import { Button } from './Button';
import { CardSection } from './CardSection';

const Confirm = ({children, onAccept, onDecline, visible}) => {
  return (
    <Modal
      visible={visible}
      transparent
      animationType="slide"
      onRequestClose={() => {}}
    >
      <View style={styles.container}>
        <CardSection style={styles.card}>
          <Text style={styles.text}>{children}</Text>
        </CardSection>
        <CardSection style={styles.card}>
          <Button onPress={onAccept}>Yes</Button>
          <Button onPress={onDecline}>No</Button>
        </CardSection>
      </View>
    </Modal>
  );
};

const styles = {
  card: {
    justifyContent: 'center',
  },
  text: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 40,
  },
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    flex: 1,
    justifyContent: 'center',
  },
};

export { Confirm };
