import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

class Section extends Component {
  render() {
    return (
      <View style={styles.container}>
        {this.props.children}
      </View>
    );
  }
};

const styles = {
  container: {
    marginTop: 15,
    marginBottom: 15,
  },
};

export { Section };