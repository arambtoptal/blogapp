import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

const CardSection = (props) => {
  return (
    <View style={[styles.containerStyle, props.style]}>
      { props.children }
    </View>
  );
};

const styles = {
  containerStyle: {
    borderBottomWidth: 1,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 12,
    paddingRight: 12,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#DDD',
    position: 'relative',
  },
};

export { CardSection };
