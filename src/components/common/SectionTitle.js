import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

class SectionTitle extends Component {
  render() {
    return (
      <Text style={styles.title}>{this.props.text}</Text>
    );
  }
};

const styles = {
  title: {
    fontSize: 20,
    paddingTop: 10,
    paddingRight: 10,
    paddingLeft: 10,
    paddingBottom: 6,
  },
};

export { SectionTitle };
