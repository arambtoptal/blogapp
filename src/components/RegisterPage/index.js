import React, { Component } from 'react';
import { Text, ScrollView } from 'react-native';
import ProfileForm from '../ProfileForm';
import { Card, CardSection, Button, Spinner, SectionTitle } from '../common';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { registerValueEnter, register } from '../../redux/actions';

class RegisterPage extends Component {
  onPressRegister() {
    const { email, password } = this.props;
    this.props.register({ email, password });
  }

  renderError() {
    if (this.props.error) {
      return (
        <CardSection>
          <Text style={{ fontSize: 16, color: '#FF0000' }}>{this.props.error}</Text>
        </CardSection>
      );
    }
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner />
    }

    return (
      <CardSection>
        <Button onPress={this.onPressRegister.bind(this)}>
          Create Account
        </Button>
      </CardSection>
    );
  }

  render() {
    return (
      <ScrollView>
        <SectionTitle text="Register" />
        <Card>
          <ProfileForm {...this.props} />
          { this.renderError() }
          { this.renderButton() }
        </Card>
      </ScrollView>
    );
  }
};

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    password: state.auth.password,
    loading: state.auth.loading,
    error: state.auth.error,
  };
}

export default connect(mapStateToProps, { registerValueEnter, register })(RegisterPage);
