import React, { Component } from 'react';
import { Text, View, FlatList, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { postsFetch, topicsFetch } from '../../redux/actions';
import _ from 'lodash';
import BlogPost from '../BlogPost';
import { Card, CardSection, Spinner } from '../common';
import firebase from 'firebase';

class AllPostsPage extends Component {
  componentDidMount() {
    this.props.postsFetch();
    this.props.topicsFetch();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedTopic !== nextProps.selectedTopic) {
      this.props.postsFetch(nextProps.selectedTopic);
    }
  }

  renderPost(post) {
    return <BlogPost post={post} />
  }

  renderPosts() {
    if (this.props.postsLoading || this.props.topicsLoading) {
      return <Spinner />
    }

    if (this.props.postsError || this.props.topicsError) {
      return (
        <Card>
          <CardSection>
            <Text style={{ fontSize: 20 }}>Error</Text>
          </CardSection>
          <CardSection>
            <Text>An error occurred while trying to fetch the posts and topics.</Text>
          </CardSection>
        </Card>
      );
    }

    if (this.props.posts.length) {
      return (
        <FlatList data={this.props.posts} renderItem={post => this.renderPost(post.item)} style={{ marginBottom: 10 }} />
      );
    }

    return (
      <Card>
        <CardSection>
          <Text style={{ fontSize: 20 }}>No posts</Text>
        </CardSection>
        <CardSection>
          <Text>There are no posts to show.</Text>
        </CardSection>
      </Card>
    );
  }

  render() {
    return (
      <ScrollView>
        {this.renderPosts()}
      </ScrollView>
    );
  }
};

const mapStateToProps = state => {
  const posts = _.map(state.posts.posts, (val, postId) => {
    return { ...val, key: postId };
  });

  const topics = _.map(state.topics.topics, (val, topicId) => {
    return { ...val, key: topicId };
  })

  return {
    posts: _.reverse(posts),
    postsLoading: state.posts.loading,
    postsError: state.posts.error,
    topics: _.reverse(topics),
    topicsLoading: state.topics.loading,
    topicsError: state.topics.error,
    selectedTopic: state.topics.selectedTopic,
  };
};

export default connect(mapStateToProps, { postsFetch, topicsFetch })(AllPostsPage);
