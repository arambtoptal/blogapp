import React, { Component } from 'react';
import {
  View,
  Text,
  Alert,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Card, CardSection, Button } from '../common';
import BlogPostForm from '../BlogPostForm';
import { connect } from 'react-redux';
import { postUpdate, postValueUpdate } from '../../redux/actions';
import _ from 'lodash';

class BlogPostUpdate extends Component {
  componentWillMount() {
    _.each(this.props.post, (value, prop) => {
      this.props.postValueUpdate({ prop, value });
    });
  }

  onPressUpdate() {
    // Make sure that the title and body are filled in.
    if (!this.props.postForm.title.length || !this.props.postForm.body.length) {
      Alert.alert(
        'Form error',
        'Post title and body fields are required.',
        [{ text: 'OK', style: 'cancel' }],
      );
    }
    else {
      const { title, image, body, topicId } = this.props.postForm;
      this.props.postUpdate({ postId: this.props.postForm.key, title, image, body, topicId });
    }
  }

  onPressDelete() {
    Alert.alert(
      'Are you sure?',
      'This will permanently delete the post.',
      [
        { text: 'Delete', onPress: () => this.onPressConfirmDelete() },
        { text: 'Cancel', style: 'cancel' }
      ]
    );
  }

  onPressConfirmDelete() {
    this.props.postDeleteData({ postId: this.props.post.key, image: this.props.post.image });
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <Card>
          <BlogPostForm imageURL={ this.props.imageURL } />
          <CardSection>
            <Button onPress={ this.onPressUpdate.bind(this) }>
              Update
            </Button>
          </CardSection>
          <CardSection>
            <TouchableOpacity style={ styles.deleteContainer } onPress={ this.onPressDelete.bind(this) }>
              <Text style={ styles.deleteText }>
                Delete
              </Text>
            </TouchableOpacity>
          </CardSection>
        </Card>
      </ScrollView>
    );
  }
};

const styles = {
  deleteContainer: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#FFF',
  },
  deleteText: {
    alignSelf: 'center',
    color: '#ba3232',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
  }
}

const mapStateToProps = state => {
  return {
    postForm: state.postForm,
    topics: state.topics,
  };
}

export default connect(mapStateToProps, { postUpdate, postValueUpdate })(BlogPostUpdate);
