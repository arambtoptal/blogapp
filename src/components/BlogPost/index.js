import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Card, CardSection, Spinner } from '../common';
import firebase from 'firebase';
import { topicsFetch } from '../../redux/actions';
import { connect } from 'react-redux';

class BlogPost extends Component {
  state = {
    loading: true,
    topicTitle: null,
    imageURL: null,
  }

  componentWillMount() {
    const { post } = this.props;
    if (post.image !== undefined) {
      this.setState({ loading: false });
      firebase.storage().ref('/' + post.image).getDownloadURL()
        .then(url => {
          this.setState({imageURL: url, loading: false});
        })
        .catch(error => console.log('firebase error fetching the post image', error, post));
    }
    else {
      this.setState({loading: false});
    }
  }

  onPressEdit() {
    const { post } = this.props;
    Actions.editPost({ post, imageURL: this.state.imageURL });
  }

  renderEditButton() {
    const { currentUser } = firebase.auth();
    const { post } = this.props;

    if (currentUser !== null && currentUser.uid === post.uid) {
      return (
        <CardSection>
          <TouchableOpacity style={styles.editButton} onPress={this.onPressEdit.bind(this)}>
            <Text style={styles.editButtonText}>Edit</Text>
          </TouchableOpacity>
        </CardSection>
      );
    }
  }

  renderImage() {
    const { post } = this.props;
    if (post.image !== undefined && post.image !== null && this.state.imageURL !== null) {
      return (
        <CardSection>
          <Image source={{uri: this.state.imageURL}} style={styles.image} />
        </CardSection>
      );
    }

    return <View />
  }

  render() {
    const { post } = this.props;

    if (this.state.loading) {
      return (
        <Card>
          <CardSection>
            <Spinner />
          </CardSection>
        </Card>
      );
    }

    let topicTitle = this.props.topics[post.topicId].title;

    return (
      <Card>
        <CardSection>
          <Text style={styles.topic}>{ topicTitle }</Text>
        </CardSection>
        {this.renderImage()}
        <CardSection>
          <View style={styles.contentWrap}>
            <Text style={styles.title}>{post.title}</Text>
            <Text style={styles.body}>{post.body}</Text>
          </View>
        </CardSection>
        {this.renderEditButton()}
      </Card>
    );
  }
}

const styles = {
  topic: {
    color: '#3a8fc4',
    paddingLeft: 10,
  },
  title: {
    fontSize: 18,
    paddingBottom: 5,
  },
  body: {
    flex: 1,
  },
  contentWrap: {
    paddingTop: 8,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
  },
  editButton: {
    flex: 1,
    paddingLeft: 10,
  },
  editButtonText: {
    color: '#888',
  },
  image: {
    flex: 1,
    height: 100,
  },
};

const mapStateToProps = state => {
  return {
    topics: state.topics.topics,
  }
}

export default connect(mapStateToProps, { topicsFetch })(BlogPost);