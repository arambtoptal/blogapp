import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import LoginForm from '../LoginForm';
import ProfileForm from '../ProfileForm';
import { Card, SectionTitle } from '../common';
import { Actions } from 'react-native-router-flux';

class LoginPage extends Component {
  render() {
    return (
      <ScrollView>
        <SectionTitle text="Login" />
        <Card>
          <LoginForm />
        </Card>
      </ScrollView>
    );
  }
};

export default LoginPage;
