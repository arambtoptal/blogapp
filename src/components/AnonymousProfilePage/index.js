import React, { Component } from 'react';
import { ScrollView, Text } from 'react-native';
import { Button, Card, CardSection } from '../common';
import { Actions } from 'react-native-router-flux';

class AnonymousProfilePage extends Component {
  render() {
    return (
      <ScrollView>
        <Card>
          <CardSection>
            <Text style={styles.title}>Login</Text>
          </CardSection>
          <CardSection>
            <Text>If you already have an account, tap below to login.</Text>
          </CardSection>
          <CardSection>
            <Button onPress={() => Actions.login()}>
              <Text>Sign In</Text>
            </Button>
          </CardSection>
        </Card>
        <Card>
          <CardSection>
            <Text style={styles.title}>Register</Text>
          </CardSection>
          <CardSection>
            <Text>If you want to create a new account, tap below to sign up.</Text>
          </CardSection>
          <CardSection>
            <Button onPress={() => Actions.register()}>
              <Text>Sign Up</Text>
            </Button>
          </CardSection>
        </Card>
      </ScrollView>
    );
  }
};

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
  },
}

export default AnonymousProfilePage;