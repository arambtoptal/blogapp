import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  Picker,
  Image,
  Platform,
  TouchableOpacity,
} from 'react-native';
import { Card, CardSection, Input, Button, Spinner } from '../common';
import { connect } from 'react-redux';
import { postValueUpdate, topicsFetch } from '../../redux/actions';
import ImagePicker from 'react-native-image-picker';
import _ from 'lodash';
import RNFetchBlob from 'rn-fetch-blob';
import firebase from 'firebase';

// Blob support.
const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;

class BlogPostForm extends Component {
  state = {
    uploadedImage: null,
  }

  uploadImage(imageObject) {
    const { currentUser } = firebase.auth();

    return new Promise((resolve, reject) => {
      const uploadUri = Platform.OS === 'ios' ? imageObject.uri.replace('file://', '') : imageObject.uri;
      let uploadBlob = null;

      const timestamp = Math.round((new Date()).getTime() / 1000);
      const fileExtensionDotPosition = imageObject.fileName.lastIndexOf('.');
      const fileExtensionLength = imageObject.fileName.length - fileExtensionDotPosition;
      const fileExtension = imageObject.fileName.toLowerCase().substr(- fileExtensionLength);
      const fileName = imageObject.fileName.substr(0, fileExtensionDotPosition) + '-' + timestamp + fileExtension;

      let contentType;
      switch (fileExtension) {
        case '.jpg':
        case '.jpeg':
          contentType = 'image/jpeg';
          break;

        case '.gif':
          contentType = 'image/gif';
          break;

        case '.tiff':
          contentType = 'image/tiff';
          break;

        case '.heic':
          contentType = 'image/heic';
          break;

        default:
          contentType = 'image/jpeg';
          break;
      }
      const imageRef = firebase.storage().ref(`/post-images/${currentUser.uid}`).child(fileName);
      fs.readFile(uploadUri, 'base64')
        .then(data => {
          return Blob.build(data, { type: `image/png;BASE64` });
        })
        .then(blob => {
          uploadBlob = blob;
          return imageRef.put(blob, { contentType: 'image/png' });
        })
        .then(imageData => {
          uploadBlob.close();
          const url = imageRef.getDownloadURL();
          return { url, imageData };
        })
        .then(url => {
          resolve(url)
        })
        .catch(error => reject(error));
    });
  }

  onPressUpload() {
    ImagePicker.showImagePicker({}, pickedImageResponse => {
      if (pickedImageResponse.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (pickedImageResponse.error) {
        console.log('ImagePicker Error: ', pickedImageResponse.error);
      }
      else if (pickedImageResponse.customButton) {
        console.log('User tapped custom button: ', pickedImageResponse.customButton);
      }
      else {
        this.uploadImage(pickedImageResponse)
          .then(uploadedImageResponse => {
            const { url, imageData } = uploadedImageResponse;
            this.setState({ uploadedImage: { 'uri': 'data:image/jpeg;base64,' + pickedImageResponse.data } });
            this.props.postValueUpdate({ prop: 'image', value: imageData });
          })
          .catch(error => console.log('an error occurred:', error));
      }
    });
  }

  onPressRemove() {
    this.setState({ uploadedImage: null });
    this.props.postValueUpdate({ prop: 'image', value: null });
  }

  renderImagePreview() {
    if (this.state.uploadedImage !== null) {
      return <Image source={ this.state.uploadedImage } style={ styles.thumb } />
    }

    if (
      this.props.imageURL !== undefined &&
      this.props.imageURL !== null &&
      this.state.uploadedImage === null &&
      (this.props.postForm === undefined || this.props.postForm.image !== null)
    ) {
      return <Image source={{ uri: this.props.imageURL }} style={ styles.thumb } />
    }

    return <Image source={ require('../../assets/image-placeholder.png') } style={ styles.thumb } />;
  }

  renderImageAction() {
    if (this.props.postForm.image !== null) {
      return (
        <View>
          <View>
            <TouchableOpacity onPress={ this.onPressUpload.bind(this) }>
              <Text>Replace image</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={ this.onPressRemove.bind(this) }>
              <Text>Remove image</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }

    return (
      <TouchableOpacity onPress={ this.onPressUpload.bind(this) }>
        <Text>Upload image</Text>
      </TouchableOpacity>
    );
  }

  render() {
    let pickerOptions = this.props.topics.map((topic, index) => {
      return <Picker.Item value={ topic.key } key={ topic.key } label={ topic.title } />
    });
    let selectedTopic = this.props.topics[0];
    if (this.props.postForm !== undefined && this.props.postForm.topicId !== null) {
      selectedTopic = this.props.postForm.topicId;
    }

    return (
      <Card>
        <CardSection>
          <Input
            label="Title"
            placeholder="Something Amazing"
            onChangeText={ text => this.props.postValueUpdate({ prop: 'title', value: text }) }
            value={ this.props.postForm.title }
          />
        </CardSection>
        <CardSection>
          <Text style={ styles.label }>Body</Text>
          <View style={{ flex: 2, height: 160 }}>
            <TextInput
              placeholder="Write away!"
              onChangeText={ text => this.props.postValueUpdate({ prop: 'body', value: text }) }
              value={ this.props.postForm.body }
              multiline
            />
          </View>
        </CardSection>
        <CardSection>
          <View style={ styles.customContainer }>
            <Text style={ styles.label }>Topic</Text>
            <Picker
              selectedValue={ selectedTopic }
              style={ styles.input }
              onValueChange={ (value, index) => this.props.postValueUpdate({ prop: 'topicId', value: value }) }
              itemStyle={{ height: 132 }}
            >
              { pickerOptions }
            </Picker>
          </View>
        </CardSection>
        <CardSection>
          <View style={ styles.customContainer }>
            <Text style={ styles.label }>Image</Text>
            <View style={{ flex: 2 }}>
              <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                <View>{ this.renderImagePreview() }</View>
                <View>{ this.renderImageAction() }</View>
              </View>
            </View>
          </View>
        </CardSection>
      </Card>
    );
  }
};

const styles = {
  label: {
    fontSize: 18,
    paddingLeft: 5,
    flex: 1,
  },
  input: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    flex: 2,
  },
  customContainer: {
    height: 142,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  thumb: {
    width: undefined,
    height: 100,
    marginBottom: 8,
  },
}

const mapStateToProps = state => {
  const topics = _.map(state.topics.topics, (val, topicId) => {
    return { ...val, key: topicId };
  })

  return {
    postForm: state.postForm,
    topics: _.reverse(topics)
  }
}

export default connect(mapStateToProps, { postValueUpdate, topicsFetch })(BlogPostForm);
