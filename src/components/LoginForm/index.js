import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { CardSection, Input, Button, Spinner } from '../common';
import { connect } from 'react-redux';
import { loginValueEnter, login } from '../../redux/actions';

class LoginForm extends Component {
  onEmailEnter(text) {
    this.props.loginValueEnter({prop: 'email', value: text});
  }

  onPasswordEnter(text) {
    this.props.loginValueEnter({prop: 'password', value: text});
  }

  onPressLogin() {
    const { email, password } = this.props;
    this.props.login({ email, password });
  }

  renderError() {
    if (this.props.error) {
      return (
        <CardSection>
          <Text style={{ fontSize: 16, color: '#FF0000' }}>{this.props.error}</Text>
        </CardSection>
      );
    }
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner />
    }

    return (
      <CardSection>
        <Button onPress={this.onPressLogin.bind(this)}>
          Login
        </Button>
      </CardSection>
    );
  }

  render() {
    return (
      <View>
        <CardSection>
          <Input
            label="Email"
            placeholder="email@address.com"
            onChangeText={this.onEmailEnter.bind(this)}
            value={this.props.email}
            autoCapitalize='none'
          />
        </CardSection>
        <CardSection>
          <Input
            label="Password"
            placeholder="password"
            onChangeText={this.onPasswordEnter.bind(this)}
            value={this.props.password}
            autoCapitalize='none'
            secureTextEntry
          />
        </CardSection>
        { this.renderError() }
        { this.renderButton() }
      </View>
    );
  }
};

const styles = {
  errorContainer: {
    background: '#FFF',
  },
  errorText: {
    paddingTop: 10,
    paddingBottom: 10,
    color: '#FF0000',
    fontSize: 20,
    alignSelf: 'center',
  },
};

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    password: state.auth.password,
    error: state.auth.error,
    loading: state.auth.loading,
  }
}

export default connect(mapStateToProps, { loginValueEnter, login })(LoginForm);